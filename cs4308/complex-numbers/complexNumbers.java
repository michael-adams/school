// Michael Adams
// Perform complex number operations
import java.util.*;

class ComplexNumber {

  // Variables for the real and imaginary numbers
  int real, imaginary;

  // Constructor which will hold the real and imaginary number variables
  public ComplexNumber(int r, int i) {
    this.real = r;
    this.imaginary = i;
  };

  // Addition
  public static ComplexNumber add(ComplexNumber numberOne, ComplexNumber numberTwo) {
    // Create empty complex number
    ComplexNumber result = new ComplexNumber(0, 0);

    // Add real numbers and store
    result.real = numberOne.real + numberTwo.real;
    // Add imaginary numbers and store
    result.imaginary = numberOne.imaginary + numberTwo.imaginary;

    // Return the result of the addition
    return result;
  }

  // Subtraction
  public static ComplexNumber subtract(ComplexNumber numberOne, ComplexNumber numberTwo) {
    // Does the same thing as the addition method but subtracts
    ComplexNumber result = new ComplexNumber(0, 0);
    result.real = numberOne.real - numberTwo.real;
    result.imaginary = numberOne.imaginary - numberTwo.imaginary;
    return result;
  }

  // Multiplication
  public static ComplexNumber multiply(ComplexNumber numberOne, ComplexNumber numberTwo) {
    ComplexNumber result = new ComplexNumber(0, 0);

    // Kinda similar to the FOIL method, remember i^2 = -1
    int realReal = numberOne.real * numberTwo.real;
    int realImaginary = numberOne.real * numberTwo.imaginary;
    int imaginaryReal = numberOne.imaginary * numberTwo.real;
    int imaginaryImaginary = numberOne.imaginary * numberTwo.imaginary * (-1);

    result.real = realReal + imaginaryImaginary;
    result.imaginary = realImaginary + imaginaryReal;
    return result;
  }

  public static void divide(ComplexNumber numberOne, ComplexNumber numberTwo) {

    // multiply numerator and denominator by the complex conjudate, expand and simplify
    ComplexNumber complexConjugate = new ComplexNumber(numberTwo.real, (numberTwo.imaginary * (-1)));
    ComplexNumber numerator = multiply(numberOne, complexConjugate);
    ComplexNumber denominator = multiply(numberTwo, complexConjugate);

    // have to print the statement here due to the multiple different ComplexNumbers being used
    System.out.println(numberOne.real + "+" + numberOne.imaginary + "i / " + numberTwo.real + "+" + numberTwo.imaginary + "i = " + numerator.real + "/" + denominator.real + " + " + numerator.imaginary + "/" + denominator.imaginary + "i");
  }

  public static void main(String args[]) {

    // Allows creation of random integers
    Random random = new Random();

    // Create 2 complex numbers using random integers
    ComplexNumber complexNumberOne = new ComplexNumber(random.nextInt(9), random.nextInt(9));
    ComplexNumber complexNumberTwo = new ComplexNumber(random.nextInt(9), random.nextInt(9));
    // Perform operations
    ComplexNumber addition = add(complexNumberOne, complexNumberTwo);
    ComplexNumber subtraction = subtract(complexNumberOne, complexNumberTwo);
    ComplexNumber multiplication = multiply(complexNumberOne, complexNumberTwo);
    divide(complexNumberOne, complexNumberTwo);

    // Strings to make it easier to print everything
    String complexNumberOneString = complexNumberOne.real + "+" + complexNumberOne.imaginary + "i";
    String complexNumberTwoString = complexNumberTwo.real + "+" + complexNumberTwo.imaginary + "i";

    // Prints out the operations
    System.out.println(complexNumberOneString + " + " + complexNumberTwoString + " = " + addition.real + "+" + addition.imaginary + "i");
    System.out.println(complexNumberOneString + " - " + complexNumberTwoString + " = " + subtraction.real + "+" + subtraction.imaginary + "i");
    System.out.println(complexNumberOneString + " * " + complexNumberTwoString + " = " + multiplication.real + "+" + multiplication.imaginary + "i");

  }
}
